

#define MAX 180
#ifndef alumnos_h
#define alumnos_h
struct alumnos
{
    int edad;
    char nombre[120];
    char genero;
    char carrera[50];
    char nCuenta[10];
    float promedio;
};
typedef struct alumnos ALUMNO;

int indiceAlArreglo=0; // esta variable global se usar· para almacenar el indice del ultimo alumno ingresado al arreglo.
ALUMNO listaAlumnos[MAX]; // declaraciÛn de un arreglo de alumnos de 180 elementos


/**  Funcion insertarAlumno
 *
 * Descripci�n:
 *   Funcion para insertar al arreglo un nuevo alumno,
 * Entrada:
 *     la funci�n  recibe un alumno "al" de entrada.
 * L�gica interna:
 *     Verifica que el indice este en el rango adecuado, entre 0 y MAX.
 *     de cumplirse ingresa el alumno al arreglo e incrementa el indice general
 * Valor de retorno:
 *     No regresa nada.
 */
void insertarAlumno(ALUMNO al){
    if (indiceAlArreglo >= 0 && indiceAlArreglo < MAX) { // verificamos que inidice este en los rangos correctos
        listaAlumnos[indiceAlArreglo]=al;
        indiceAlArreglo++;
    }else{
        printf("El indice apunta fuera del arreglo, favor de revisar la l�gica");
    }
}

/** Funcion nuevoAlumno
 * Descripci�n:
 *   Funcion para capturar los datos desde teclado de un nuevo alumno,
 * Entrada:
 *     la funci�n no recibe ningun par�metro de entrada.
 * L�gica interna:
 *     Crea una variable temporal del tipo alumno y se obtienen los datos desde el teclado.
 *     Tamb�en se agrega el alumno al arreglo
 * Valor de retorno:
 *     Regreza la variable temporal de tipo ALUMNO con los datos llenos.
 */
ALUMNO nuevoAlumno(){
    ALUMNO tmp;
    printf("Introduce la edad:");
    scanf("%d",&tmp.edad);
    printf("Introduce el genero [M o F]:");
    scanf(" %c",&tmp.genero);   // El espacio antes del %c es para que ignore espacios en blanco
    printf("Introduce el nombre:");
    scanf("%*c%[^\n]",tmp.nombre);
    printf("Introduce Carrera:");
    scanf("%*c%[^\n]",tmp.carrera);
    printf("Introduce numero de cuenta:");
    scanf("%*c%[^\n]",tmp.nCuenta);
    printf("Introduce tu promedio:");
    scanf("%f",&tmp.promedio);
    /*
     * Aqui agregamos al alumno al arreglo e incrementamos el �ndice para que apunte al siguiente elementos
     */
    insertarAlumno(tmp);
    return tmp;
}
/** funcion imprimeAlumno
 * Descripci�n:
 *   Funcion para imprimir en pantalla los datos de un solo alumno,
 * Entrada:
 *     la funci�n  recibe la variable alumno a ser impresa.
 * L�gica interna:
 *     S�lo imprime cada uno de los campos
 * Valor de retorno:
 *     No regresa nada
 */
void imprimeAlumno(ALUMNO alu){
    printf("\tNombre:%s\n",alu.nombre);
    printf("\tEdad:%d\n",alu.edad);
    printf("\tGenero:%c\n",alu.genero);
    printf("\tCarrera:%s\n",alu.carrera);
    printf("\tNumero de Cuenta:%s\n",alu.nCuenta);
    printf("\tPromedio:%.2f\n",alu.promedio);
    printf("+---------------------------------+\n\n");
}

/** funcion imprimirLista
 * Descripci�n:
 *   Funcion para imprimir en pantalla TODOS los datos del arreglo.
 * Entrada:
 *     la funci�n NO recibe parametros de entrada.
 * L�gica interna:
 *     en un for recorre el arreglo hasta el indiceAlArreglo que es el que almacena el tope actual
 * Valor de retorno:
 *     No regresa nada
 */
void imprimirLista(){
    int j=0;
    for (j = 0; j < indiceAlArreglo; j++) {
        printf("+--------- # de lista: %d ---------+*\n",j+1);
        imprimeAlumno(listaAlumnos[j]);
    }
}



int menu(){
    int op=0;
    printf("\n----------- Men� para la aplicacion de BD para alumnos ---------\n");
    printf("(1) Crear lista.\n");
    printf("(2) Guardar a archivo.\n");
    printf("(3) Leer desde archivo.\n");
    printf("(4) Mostrar lista.\n");
    printf("(5) Agregar alumno. \n");
    printf("(6) Obtener promedio de alumnos.\n");
    printf("(7) Buscar alumno por nombre. \n");
    printf("(8) Buscar alumno por edad. \n");
    printf("(9) Eliminar alumno. \n");
    printf("(10) Modificar datos \n");
    printf("(11) Uso futuro \n");
    printf("(12) Uso futuro \n");
    printf("(0) SALIR\n");
    printf("\n\nElige una opcion:");
    scanf("%d",&op);

    return op;
}



/**
Manejo de archivos
*/

/*
 Funcion para grabar un ARREGLO DE REGISTROS
 en el archivo Evaluacion.dat
 */
void grabaRegistros(ALUMNO r[], int tam){
    FILE *ptrF;

    if((ptrF=fopen("Evaluacion.dat","w"))==NULL){
        printf("el archivo no se puede abrir\n");
    }else{
        fwrite(r,sizeof(ALUMNO),tam,ptrF);
    }

    fclose(ptrF);
}

/*
 Funcion para LEER  REGISTROs
 en el archivo Evaluacion.dat
 */
void leerRegistros(int tam){

    FILE *ptrF;

    if((ptrF=fopen("Evaluacion.dat","rb"))==NULL){
        printf("el archivo no se puede abrir\n");
    }
    else{
        //for /*(int i=0;i<tam;i++)*/
        fread(listaAlumnos,sizeof(ALUMNO),tam,ptrF);
    }

    fclose(ptrF);
}

/**
 *
 * Regresa el numero de registros almacenados en el archivo
 *
 */
int registrosEnArchivo(){
    FILE *ptrF;
    int contador=0;
    ALUMNO  basura;
    if((ptrF=fopen("Evaluacion.dat","rb"))==NULL){
        printf("el archivo no se puede abrir\n");
    }
    else{
        while(!feof(ptrF)){
            if (fread(&basura,sizeof(ALUMNO),1,ptrF))
                contador++;
        }

    }
    fclose(ptrF);
    return contador;
}
float PromedioAlumno(){
  float suma = 0;
  float promedio = 0;
  ALUMNO alumno;
      for (int i=0;i<indiceAlArreglo;i++) {
        alumno = listaAlumnos[i];
        suma += alumno.promedio;
      }
        promedio=suma/indiceAlArreglo;
    printf("El promedio de los alumnos es: %0.2f \n", promedio);

 return promedio;
}

void BuscarNombre(){
  char buscaNombre[50];
  ALUMNO buscaAlumno;
printf("Ingrese el nombre a buscar: " );
scanf("%*c%[^\n]",buscaNombre );
printf("+---------------------------------+\n\n" );
 for (int i=0;i<indiceAlArreglo;i++) {
   buscaAlumno=listaAlumnos[i];
   strcmp(buscaAlumno.nombre,buscaNombre);
 if (strcmp(buscaAlumno.nombre,buscaNombre)==0) {
   imprimeAlumno(buscaAlumno);
    }
  }

}
void BuscarEdad() {
  int buscarEdad;
  ALUMNO edadAlumno;
printf("Ingresa la edad que deseas buscar: " );
scanf("%d",&buscarEdad );
printf("+---------------------------------+\n\n" );
 for (int i=0;i<indiceAlArreglo;i++) {
   edadAlumno=listaAlumnos[i];
 if (edadAlumno.edad==buscarEdad) {
   imprimeAlumno(edadAlumno);
    }
  }
}

ALUMNO EliminarAlumno(){
  int z;
  ALUMNO borrarAlumno;
  printf("Ingrese el No. de Lista de alumno a eliminar: ");
  scanf("%d",&z );
  z--;
     if (z>=0 && z<indiceAlArreglo) {
       borrarAlumno=listaAlumnos[z];
       for (int i=z;i<indiceAlArreglo;i++) {
         listaAlumnos[i]=listaAlumnos[i+1];
       }
       indiceAlArreglo--;
     } else{
       printf("No existe el No. de Lista.\n" );
     }
     printf("Alumno eliminado: \n" );
     imprimeAlumno(borrarAlumno);
     return borrarAlumno;
}

ALUMNO EditarAlumno(){
  int a;
  int o;
  ALUMNO editarAlumno;
  printf("Ingrese el No. de Lista del alumno a editar: ");
   scanf("%d",&a );
   a--;
   if (a>=0 && a<indiceAlArreglo) {
     editarAlumno=listaAlumnos[a];
     printf("Editar Alumno:\n" );
       imprimeAlumno(editarAlumno);
     printf("+---------------------------------+\n\n");
      printf("Campos:\n" );
        printf("(1) Edad.\n" );
        printf("(2) Género.\n" );
        printf("(3) Nombre.\n" );
        printf("(4) Carrera.\n" );
        printf("(5) No. de Cuenta.\n" );
        printf("(6) Promedio.\n" );
     printf("Seleccione el campo: " );
      scanf("%d",&o );
      printf("+---------------------------------+\n\n" );
      switch (o) {
        case 1:
          printf("La edad actual es: %d\n",editarAlumno.edad );
           printf("Actualice edad: " );
            scanf("%d",&editarAlumno.edad );
        printf("El nuevo estado del alumno %d es:\n",a );
        imprimeAlumno(editarAlumno);

        break;
        case 2:
          printf("El género actual es: %c\n",editarAlumno.genero );
           printf("Actualice género: " );
            scanf(" %c",&editarAlumno.genero );
        printf("El nuevo estado del alumno %d es:\n",a );
        imprimeAlumno(editarAlumno);

        break;
        case 3:
          printf("El nombre actual es: %s\n",editarAlumno.nombre );
           printf("Actualice nombre: " );
            scanf("%*c%[^\n]",editarAlumno.nombre );
        printf("El nuevo estado del alumno %d es:\n",a );
        imprimeAlumno(editarAlumno);
        printf("Presione (2) para guardar los datos actualizados.\n" );
        break;
        case 4:
          printf("La carrera actual es: %s\n",editarAlumno.carrera );
           printf("Actualice carrera: " );
            scanf("%*c%[^\n]",editarAlumno.carrera );
        printf("El nuevo estado del alumno %d es:\n",a+1 );
        imprimeAlumno(editarAlumno);

        break;
        case 5:
          printf("El No. de Cuenta actual es: %s\n",editarAlumno.nCuenta );
           printf("Actualice No. de Cuenta: " );
            scanf("%*c%[^\n]",editarAlumno.nCuenta );
        printf("El nuevo estado del alumno %d es:\n",a+1 );
        imprimeAlumno(editarAlumno);

        break;
        case 6:
          printf("El promedio actual es: %f\n",editarAlumno.promedio );
           printf("Actualice promedio: " );
            scanf("%f",&editarAlumno.promedio );

        printf("El nuevo estado del alumno %d es:\n",a+1 );
        imprimeAlumno(editarAlumno);
      break;
      default:
       printf("Opción inválida.\n" );
      break;
      }
   } else {
     printf("No existe el No. de Lista.\n" );
   }
  listaAlumnos[a]=editarAlumno;
return editarAlumno;
}

#endif /* alumnos_h */

